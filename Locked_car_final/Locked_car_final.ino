

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include<SoftwareSerial.h> //Receber Texto
#include<TinyGPS.h> //Ler 
#include <SerialRelay.h>

LiquidCrystal_I2C lcd(0x20,16,2);  // Criando um LCD de 16x2 no endereço 0x20
SoftwareSerial serial1(9,8);//gps(Tx)->(9) / gps(Rx)-> (8)
SoftwareSerial mySerial(10, 11);  // GSM

TinyGPS gps1;
const byte NumModules = 1;
SerialRelay relays(4,5,NumModules); // (data, clock, number of modules)

const int buttonPin2 = 2;  
const int buttonPin3 = 3; 
int buttonState2 = 0;  
int buttonState3 = 0;  

//String URL;
//String RestoUrl;
const int buttonPin = 2; 
int buttonState = 0;  

String apn = "gprs.oi.com.br";                       //APN
String apn_u = "oi";                     //APN-Username
String apn_p = "oi";                     //APN-Password
String url;
String data;   //String for the first Paramter (e.g. Sensor1)
String horario;

void setup()
{
//  lcd.init();                 // Inicializando o LCD
//  lcd.backlight();            // Ligando o BackLight do LCD
//  lcd.print("Victor"); // Exibindo no LED Hello, world!

  serial1.begin(9600);
  Serial.begin(9600);

  analogReference(INTERNAL);  // Muda a referência de 5V para 1,1V
  lcd.init();           
  lcd.backlight(); 
  
    pinMode(buttonPin2, INPUT);
  pinMode(buttonPin3, INPUT);
  
  Iniciando();

}

void loop()
{ 
  bool recebido = false;

 buttonState2 = digitalRead(buttonPin2);
   buttonState3 = digitalRead(buttonPin3);

while(serial1.available()){
 char cIn = serial1.read();
 recebido = gps1.encode(cIn);
}
  if (recebido){
    lcd.setCursor(1,0);
    lcd.print("Welcome!");
    lcd.setCursor(1,1);
    lcd.print("Waiting comand...");
    
    VerificaButon();
  }
}

void Iniciando(){
  lcd.setCursor(0,1);
  lcd.print("Iniciando...");
  lcd.setCursor(0,1);
  delay(1000);
  lcd.print("Iniciando ..");
  lcd.setCursor(0,1);
  delay(1000);
  lcd.print("Iniciando. .");
  lcd.setCursor(0,1);
  delay(1000);
  lcd.print("Iniciando.. ");
  delay(1000);
  lcd.clear();
}

void Texto(String texto){
  lcd.clear();
  lcd.setCursor(1,0);
  lcd.print(texto);
  lcd.setCursor(1,0);
  delay(1000);
  lcd.clear();
}

void VerificaButon(){
   if (buttonState2 == HIGH) {
    Texto("Alarme Ativado");
    
  relays.SetRelay(2, SERIAL_RELAY_ON, 1);
  delay(500); 
  relays.SetRelay(3, SERIAL_RELAY_OFF, 1);// turn the relay ON
  delay(1000);      
  GPS();
  // wait for a second
    } else if (buttonState3 == HIGH) {
      Texto("Motor Ativado");
  relays.SetRelay(3, SERIAL_RELAY_ON, 1);
  delay(500); 
  relays.SetRelay(2, SERIAL_RELAY_OFF, 1);// turn the relay ON
  delay(1000);                              // wait for a second
    }
}

void GPS(){
    delay(500);
    Texto("Iniciando GPS...");
  //latitude e longitude
  long latitude , longitude;
  unsigned long idadeInfo;
  gps1.get_position(&latitude, &longitude);
  
//  if (latitude != TinyGPS::GPS_INVALID_F_ANGLE){
//  Serial.print("Latitude: ");
//  Serial.println(float(latitude)/100000,6);
//  }
//
//  if (longitude != TinyGPS::GPS_INVALID_F_ANGLE){
//  Serial.print("Longitude: ");
//  Serial.println(float(longitude)/100000,6); 
//  }  
  delay(200);
  lcd.setCursor(1,0);
    lcd.print("Long: ");
  lcd.print(float(longitude)/100000,6);
  delay(200);
     lcd.setCursor(1,1);
    lcd.print("Lat: ");
  lcd.print(float(latitude)/100000,6);
    delay(3500); 
  //  lcd.clear();
  lcd.clear();

  //dia e hora
int ano;
byte mes, dia, hora, minuto, segundo, centesimo;
gps1.crack_datetime(&ano,&mes,&dia,&hora,&minuto,&segundo,&centesimo);

//Serial.print("Data (GMT): ");
//Serial.print(dia);
//Serial.print("/");
//Serial.print(mes);
//Serial.print("/");
//Serial.println(ano);
data = String(dia) + "/" + String(mes) + "/" + String(ano);


//Serial.print("Horario(GMT): ");
//Serial.print(hora-3);
//Serial.print(":");
//Serial.print(minuto);
//Serial.print(":");
//Serial.print(segundo);
//Serial.print(":");
//Serial.println(centesimo);
//hora = String(int(hora)-3);
horario = String(hora)+":"+String(minuto)+":"+String(segundo);

  url = "http://dweet.io/dweet/for/Victor_Lima_Locked_car?lat="+String(float(latitude)/100000,6) +"&long="+ String(float(longitude)/100000,6);
  
  Texto("Iniciando GSM");

  //data1 = "123";
  //data2 = "Victor";
  delay(1000);

  gsm_sendhttp();
  

  mySerial.end();
  delay(1000);
  serial1.begin(9600);
  delay(1000);

  Texto("Terminado");
 
}

void gsm_sendhttp() {
  serial1.end();
  delay(1000);
  mySerial.begin(9600);
  delay(1000);

  Texto("Terminado");
 
  mySerial.println("AT");
  runsl();//Print GSM Status an the Serial Output;
  delay(4000);
  mySerial.println("AT+SAPBR=3,1,Contype,GPRS");
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=3,1,APN," + apn);
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=3,1,USER," + apn_u);//Comment out, if you need username
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=3,1,PWD," + apn_p); //Comment out, if you need password;
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR =1,1");
  runsl();
  delay(100);
  mySerial.println("AT+SAPBR=2,1");
  runsl();
  delay(2000);
  mySerial.println("AT+HTTPINIT");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=CID,1");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=URL," + url);
  runsl();
  delay(100);
  mySerial.println("AT+HTTPPARA=CONTENT,application/x-www-form-urlencoded");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPDATA=192,10000");
  runsl();
  delay(100);
  mySerial.println("params=" + data + "~" + horario+" ");
  runsl();
  delay(10000);
  mySerial.println("AT+HTTPACTION=1");
  runsl();
  delay(5000);
  mySerial.println("AT+HTTPREAD");
  runsl();
  delay(100);
  mySerial.println("AT+HTTPTERM");
  runsl(); 
}

//Print GSM Status
void runsl() {
  while (mySerial.available()) {
  Serial.write(mySerial.read());
  }

}


